﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace RoomMaker
{
    public class MoveThumb : Thumb
    {
        public Canvas myCanvas { get; set; }

        public MoveThumb()
        {
            DragDelta += new DragDeltaEventHandler(this.MoveThumb_DragDelta);
            //myCanvas = ((MainWindow)System.Windows.Application.Current.MainWindow).MyCanvas;
        }

        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            ContentControl designerItem = DataContext as ContentControl;

            if (designerItem != null)
            {
                Point dragDelta = new Point(e.HorizontalChange, e.VerticalChange);

                RotateTransform rotateTransform = designerItem.RenderTransform as RotateTransform;
                if (rotateTransform != null)
                {
                    dragDelta = rotateTransform.Transform(dragDelta);
                }
                double newLeft = Canvas.GetLeft(designerItem) + dragDelta.X;
                double newTop = Canvas.GetTop(designerItem) + dragDelta.Y;

                if (newLeft < 0)
                    newLeft = 0;
                else if (newLeft + designerItem.ActualWidth > SystemParameters.PrimaryScreenWidth)
                    newLeft = SystemParameters.PrimaryScreenWidth - designerItem.ActualWidth;

                if (newTop < 0)
                    newTop = 0;
                else if (newTop + designerItem.ActualHeight > SystemParameters.PrimaryScreenHeight)
                    newTop = SystemParameters.PrimaryScreenHeight - designerItem.ActualHeight;
             

                Canvas.SetLeft(designerItem, newLeft);
                Canvas.SetTop(designerItem, newTop);
                // if (Canvas.GetLeft(designerItem) > 0 && Canvas.GetTop(designerItem) > 0)
                // {
                //     Canvas.SetLeft(designerItem, Canvas.GetLeft(designerItem) + dragDelta.X);
                //     Canvas.SetTop(designerItem, Canvas.GetTop(designerItem) + dragDelta.Y);
                // }
                //else if (e.HorizontalChange>0)
                // {
                //     Canvas.SetLeft(designerItem, Canvas.GetLeft(designerItem) + dragDelta.X);

                // }
                //else if (e.VerticalChange>0)
                // {   

                //     Canvas.SetTop(designerItem, Canvas.GetTop(designerItem) +dragDelta.Y);
                // }
                //if (Canvas.GetLeft(designerItem) < 0 && Canvas.GetTop(designerItem) < 0)
                //{
                //    Canvas.SetRight(designerItem, Canvas.GetLeft(designerItem) + 10);
                //    Canvas.SetBottom(designerItem, Canvas.GetTop(designerItem) + 10);
                //}


            }
        }
    }
}
