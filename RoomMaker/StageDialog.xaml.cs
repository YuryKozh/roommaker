﻿using RoomMaker.Model;
using RoomMaker.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RoomMaker
{
    /// <summary>
    /// Логика взаимодействия для StageDialog.xaml
    /// </summary>
    /// 
    public class ListValidationRule : ValidationRule
    {


        private bool Validate(string text)
        {

            if (String.IsNullOrEmpty(text)) return false;
            foreach (var x in text)
            {
                if (!Char.IsDigit(x))
                    return false;
            }

            return true;
        }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;
            if (str == null)
            {
                return new ValidationResult(false, "Please enter some digits");

            }
            else if (!Validate(str))
            {
                return new ValidationResult(false, String.Format("Incorrect input! Digits only!"));
            }

            return new ValidationResult(true, null);

        }
    }
    public partial class StageDialog : Window
    {
      
        private readonly StageDialogViewModel _stagedialogviewmodel;
        public ObservableCollection <NumberofRooms> List { get; set; }


        public StageDialog(int number)
        {
            InitializeComponent();
            List = new ObservableCollection<NumberofRooms>();
            for(int i=0;i<number;i++)
            {
                List.Add(new NumberofRooms() { Number = 1 });
            }
            _stagedialogviewmodel = new StageDialogViewModel() { Validation = true, Values = List };
            DataContext = _stagedialogviewmodel;
          
       
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }


        public ObservableCollection<NumberofRooms> AnswerList
        {
            get { return _stagedialogviewmodel.Values; }
        }

        private void Validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added) _stagedialogviewmodel.Errors += 1;
            if (e.Action == ValidationErrorEventAction.Removed) _stagedialogviewmodel.Errors -= 1;
            if(_stagedialogviewmodel.Errors==0)
            {
                _stagedialogviewmodel.Validation = true;
            }
            else { _stagedialogviewmodel.Validation = true; }
        }
    }


    
    }
