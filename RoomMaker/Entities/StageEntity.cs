﻿using RoomMaker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Entities
{
   public class StageEntity:BaseEntity
    {
        public List<ExtendedAppartmentEntity>  AppartmentList { get; set; }
        public string Text { get; set; }
    }
}
