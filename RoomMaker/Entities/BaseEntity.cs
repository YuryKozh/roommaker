﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public int ObjectsCount { get; set; }

    }
}
