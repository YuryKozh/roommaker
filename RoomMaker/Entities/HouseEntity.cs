﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Entities
{
   public class HouseEntity:BaseEntity
    {
        public List<StageEntity> StageList { get; set; }
    }
}
