﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Entities
{
    public class RoomEntity:BaseEntity
    {

        public string RoomName { get; set; }
        public double RoomSquare { get; set; }
        public double RoomWidth { get; set; }
        public double RoomHeight { get; set; }
    }
}
