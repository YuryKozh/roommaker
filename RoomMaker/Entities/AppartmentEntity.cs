﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Entities
{
    public class AppartmentEntity : BaseEntity
    {
        public int MarginLeft { get; set; }
        public int MarginUp { get; set; }
        public int MarginDown { get; set; }
        public int MarginRight { get; set; }
        public int AppartmentWidth { get; set; }
        public int AppartmentHeight { get; set; }
        public List<RoomEntity> RoomList {get;set;}
    }
}
