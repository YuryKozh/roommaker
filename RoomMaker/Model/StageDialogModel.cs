﻿using RoomMaker.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Model
{
    public class StageDialogModel
    {
        public bool ValidateFields { get; set; }
        public ObservableCollection<NumberofRooms> List {get; set;}
        public int Errors { get; set; }
    }

    public class NumberofRooms 
    {    
        public int Number { get; set; }      
    }
}
