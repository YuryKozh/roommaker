﻿using RoomMaker.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RoomMaker.Model
{
    public class AppartmentDialogModel
    {
    public   ObservableCollection<StageAppartment> StageBase { get; set; }
    }

    public class StageAppartment
    {
        public int StageNumber { get; set; }
        public ObservableCollection<ExtendedAppartmentEntity> AppartmentBase { get; set; }
    }


    public class ExtendedAppartmentEntity:AppartmentEntity
    {
        public ICommand AddLeft { get; set; }
        public ICommand RemoveLeft { get; set; }
        public ICommand AddUp { get; set; }
        public ICommand RemoveUp { get; set; }
        public Thickness Margin { get; set; }
    }
 }