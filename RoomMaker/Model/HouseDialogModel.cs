﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
namespace RoomMaker.Model
{
    public class HouseDialogModel
    {
        public string NumberOfStages { get; set; }
        public bool isFieldDigit { get; set; }
        public string StageColors { get; set; }
        public bool IsCorrectBackground { get; set; }     
    }

    
}
