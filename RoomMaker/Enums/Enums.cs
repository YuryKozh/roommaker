﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomMaker.Enums
{
   public class Enums
    {
       public enum RoomLocation {LeftUp,RightUp,LeftDown,RightDown};
       public enum RoomSize { Standard, VerticalUnion,HorizontalUnion};
       public enum AppartmentLocation { LeftUp,RightUp,LeftDown,RightDown};
       public enum AppartmentSize { Standard,VerticalUnion,HorizonatalUnion};

    }
}
