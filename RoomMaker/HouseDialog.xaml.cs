﻿using RoomMaker.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RoomMaker
{
    /// <summary>
    /// Логика взаимодействия для HouseDialog.xaml
    /// </summary>
    public partial class HouseDialog : Window
    {
        private readonly HouseDialogViewModel _housedialogviewmodel;

        public HouseDialog(string question, string defaultAnswer = "")
        {
            InitializeComponent();
            lblQuestion.Content = question;
           // txtAnswer.Text = defaultAnswer;
            _housedialogviewmodel = new HouseDialogViewModel() { IsFieldDigit = true, StageNumber = "0",StageColorString="0000ff"};
            DataContext = _housedialogviewmodel;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            txtAnswer.SelectAll();
            txtAnswer.Focus();
        }

        public string Answer
        {
            get { return _housedialogviewmodel.StageNumber; }
        }

        public string StageColor
        {
            get { return _housedialogviewmodel.StageColor; }
        }

        
    }
}
