﻿using RoomMaker.Entities;
using RoomMaker.Model;
using RoomMaker.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RoomMaker
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class User
        {
            public string Name { get; set; }

            public int Age { get; set; }

            public string Mail { get; set; }
        }

        private readonly MainWindowViewModel _mainWindowViewModel;
        private Rectangle rect;
        private double top = 20;
        private double left = 20;
        int i = 0;
        public Grid TestGrid { get; set; }
        public MainWindow()
        {

            InitializeComponent();
            _mainWindowViewModel = new MainWindowViewModel() { IsControlOnCanvas=true };
            DataContext = _mainWindowViewModel;
        }


        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            var contentControl = new ContentControl() { Width = 100, Height = 100 };
            var textblock = new TextBlock() { Text = "room 1", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            var textblock1 = new TextBlock() { Text = "room 2", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            var textblock2 = new TextBlock() { Text = "room 3", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            var textblock3 = new TextBlock() { Text = "room 4", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            var grid = new Grid();

            ColumnDefinition c1 = new ColumnDefinition();
            ColumnDefinition c2 = new ColumnDefinition();
            RowDefinition r1 = new RowDefinition();
            RowDefinition r2 = new RowDefinition();
            c1.Width = new GridLength(25, GridUnitType.Star);
            c2.Width = new GridLength(25, GridUnitType.Star);
            r1.Height = new GridLength(25, GridUnitType.Star);
            r2.Height = new GridLength(25, GridUnitType.Star);
            grid.ColumnDefinitions.Add(c1);
            grid.ColumnDefinitions.Add(c2);
            grid.RowDefinitions.Add(r1);
            grid.RowDefinitions.Add(r2);

            Style style = this.FindResource("DesignerItemStyle") as Style;
            contentControl.Style = style;
            Rectangle rect = new Rectangle { Fill = Brushes.Aqua, IsHitTestVisible = false, Width = 50, Height = 50 };
            Rectangle rect1 = new Rectangle { Fill = Brushes.Azure, IsHitTestVisible = false, Width = 50, Height = 50 };
            Rectangle rect2 = new Rectangle { Fill = Brushes.Chocolate, IsHitTestVisible = false, Width = 50, Height = 50 };
            Rectangle rect3 = new Rectangle { Fill = Brushes.Coral, IsHitTestVisible = false, Width = 50, Height = 50 };
            contentControl.SetValue(Selector.IsSelectedProperty, true);
            Canvas.SetLeft(contentControl, 100);
            Canvas.SetTop(contentControl, 100);
            Grid.SetColumn(rect, 0);
            Grid.SetRow(rect, 0);
            Grid.SetColumn(textblock, 0);
            Grid.SetRow(textblock, 0);

            Grid.SetColumn(rect1, 1);
            Grid.SetRow(rect1, 0);
            Grid.SetColumn(textblock1, 1);
            Grid.SetRow(textblock1, 0);

            Grid.SetColumn(rect2, 0);
            Grid.SetRow(rect2, 1);
            Grid.SetColumn(textblock2, 0);
            Grid.SetRow(textblock2, 1);

            Grid.SetColumn(rect3, 1);
            Grid.SetRow(rect3, 1);
            Grid.SetColumn(textblock3, 1);
            Grid.SetRow(textblock3, 1);

            contentControl.Content = grid;
            grid.Children.Add(rect);
            grid.Children.Add(rect1);
            grid.Children.Add(rect2);
            grid.Children.Add(rect3);
            grid.Children.Add(textblock);
            grid.Children.Add(textblock1);
            grid.Children.Add(textblock2);
            grid.Children.Add(textblock3);

            TestGrid = grid;
            MyCanvas.Children.Add(contentControl);

            contentControl.MouseRightButtonDown += ContentControl_MouseRightButtonDown;


        }



        private void ContentControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ((ContentControl)sender).SetValue(Selector.IsSelectedProperty,!(bool)((ContentControl)sender).GetValue(Selector.IsSelectedProperty));

        }

      


        private void CreateHouse_Click(object sender, RoutedEventArgs e)
        {
           
            HouseDialog win2 = new HouseDialog("Number of Stages");
            if (win2.ShowDialog() == true)
            {
                StageDialog win3 = new StageDialog(Convert.ToInt32(win2.Answer));
                    if (win3.ShowDialog() == true)
                {
                    AppartmentDialog win4 = new AppartmentDialog(win3.AnswerList);

                    if (win4.ShowDialog() == true)
                    {
                        ObservableCollection<StageAppartment> AppartmentLists = win4.AppartmentLists;
                        HouseEntity house = new HouseEntity() { ObjectsCount = Convert.ToInt32(win2.Answer) };
                        
                        var housegrid = new Grid();
                        var listview = new ListView() { Width = 35, Height = 28 * (house.ObjectsCount) };
                        listview.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(win2.StageColor));
                        listview.Padding = new Thickness(0);
                        listview.Margin = new Thickness(0);
                        listview.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                        listview.VerticalContentAlignment = VerticalAlignment.Stretch;
                        listview.BorderThickness = new Thickness(0, 0, 0, 0);
                        house.StageList = new List<StageEntity>();
                        var contentControl = new ContentControl() { Width = 35, Height = 28 * (house.ObjectsCount)};
                        Style style = this.FindResource("DesignerItemStyle") as Style;
                        contentControl.Style = style;
                        var roomList = win3.AnswerList;

                        for (int i = 0; i < house.ObjectsCount; i++)
                        {
                            house.StageList.Add(new StageEntity() { AppartmentList = AppartmentLists[i].AppartmentBase.ToList(), ObjectsCount = Convert.ToInt32(roomList[i].Number), Text = (house.ObjectsCount - i) + "" });
                            //    housegrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25, GridUnitType.Star) });
                            //    Rectangle rect = new Rectangle {IsHitTestVisible = false, Width = 25, Height = 25 };
                            //    Rectangle boxview = new Rectangle { Fill = Brushes.Brown, IsHitTestVisible = false, Height = 3, VerticalAlignment = VerticalAlignment.Top };
                            //    var textblock = new TextBlock() { Text = (house.ObjectsCount - i) + "", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
                            ////    textblock.PreviewMouseLeftButtonDown += TextBlock_PreviewMouseLeftButtonDown;
                            //    Grid.SetColumn(rect, 0);
                            //    Grid.SetRow(rect, 1);
                            //    Grid.SetColumn(boxview, 0);
                            //    Grid.SetRow(boxview, 1);
                            //    Grid.SetColumn(textblock, 0);
                            //    Grid.SetRow(textblock, 1);
                            //    housegrid.Children.Add(rect);
                            //    housegrid.Children.Add(boxview);
                            //    housegrid.Children.Add(textblock);
                            // listview.Items.Add(housegrid);
                          //  housegrid = new Grid();

                        }
                        DataTemplate lvTemplate = new DataTemplate();
                        lvTemplate.DataType = typeof(StageEntity);
                        FrameworkElementFactory gridFactory = new FrameworkElementFactory(typeof(Grid));
                        gridFactory.SetValue(PaddingProperty, new Thickness(0));
                        gridFactory.Name = "myComboFactory";
                        //set up the card holder textblock
                        FrameworkElementFactory rectFactory = new FrameworkElementFactory(typeof(Rectangle));
                        var row1 = new FrameworkElementFactory(typeof(RowDefinition));

                        rectFactory.SetValue(IsHitTestVisibleProperty, false);
                        rectFactory.SetValue(WidthProperty, 25d);
                        rectFactory.SetValue(HeightProperty, 25d);
                        
                        FrameworkElementFactory boxFactory = new FrameworkElementFactory(typeof(Rectangle));
                        boxFactory.SetValue(Shape.FillProperty, new SolidColorBrush(Colors.SandyBrown));
                        boxFactory.SetValue(IsHitTestVisibleProperty, false);
                        boxFactory.SetValue(HeightProperty, 3d);
                        boxFactory.SetValue(VerticalAlignmentProperty, VerticalAlignment.Top);

                        FrameworkElementFactory txtblFactory = new FrameworkElementFactory(typeof(TextBlock));
                        txtblFactory.SetBinding(TextBlock.TextProperty, new Binding("Text"));
                        txtblFactory.SetValue(VerticalAlignmentProperty, VerticalAlignment.Center);
                        txtblFactory.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Center);
                        //var textblock = new TextBlock() { Text = "test" + "", VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
                        boxFactory.SetValue(Grid.RowProperty, 1);
                        boxFactory.SetValue(Grid.ColumnProperty, 0);
                        rectFactory.SetValue(Grid.ColumnProperty, 0);
                        rectFactory.SetValue(Grid.RowProperty, 1);
                        txtblFactory.SetValue(Grid.RowProperty, 1);
                        txtblFactory.SetValue(Grid.ColumnProperty, 0);
                        gridFactory.AppendChild(boxFactory);
                        gridFactory.AppendChild(rectFactory);
                        gridFactory.AppendChild(txtblFactory);
                        lvTemplate.VisualTree = gridFactory;
                        listview.ItemTemplate = lvTemplate;
                        listview.ItemsSource = house.StageList;
                        EventSetter ev = new EventSetter();
                        ev.Event = ListViewItem.PreviewMouseLeftButtonDownEvent;
                        ev.Handler = new MouseButtonEventHandler(itemClicked);
                        
                        Style myStyle = new Style();
                        myStyle.TargetType = typeof(ListViewItem);
                        myStyle.Setters.Add(new Setter(ListViewItem.PaddingProperty, new Thickness(0)));
                        myStyle.Setters.Add(ev);
                        listview.ItemContainerStyle = myStyle;
                        ScrollViewer.SetVerticalScrollBarVisibility(listview, ScrollBarVisibility.Hidden);
                        housegrid.PreviewMouseLeftButtonDown += Grid_PreviewMouseLeftButtonDown;
                        housegrid.MouseLeftButtonDown += Grid_MouseLeftButtonDown;
                        contentControl.Content = listview;
                        Canvas.SetLeft(contentControl, 200);
                        Canvas.SetTop(contentControl, 10);
                        MyCanvas.Children.Add(contentControl);
                        contentControl.MouseRightButtonDown += ContentControl_MouseRightButtonDown;
                    }
                }
            }
        }

        void itemClicked(object sender, MouseButtonEventArgs e)
        {
            var ListItem = (sender as ListViewItem);
            var myObject = ListItem.DataContext as StageEntity;
            // ( (RoutedEventArgs) e.Source).
            StageWindow win2 = new StageWindow(myObject.AppartmentList);
            win2.Show();
            e.Handled = false;
        }

        private void Grid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("Grid Preview");
            var grid = ((Grid)sender);
            e.Handled = false;
        }
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Grid MouseLeftButtonDown");
            //var grid = ((Grid)sender);
            //grid.Background = Brushes.Black;
         //   e.Handled = true;
        }

        private void Rect_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("rect preview");
            var grid = ((Rectangle)sender);
            grid.Fill = Brushes.Black;
            e.Handled = false;       
        }

        private void TextBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
           // MessageBox.Show("textbox preview");
            var txtblock = ((TextBlock)sender);
            txtblock.Foreground=Brushes.White;
            // MessageBox.Show("Row:" + Grid.GetRow(txtblock));
          // StageWindow win2 = new StageWindow();
          //  win2.Show();
            e.Handled = false;
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Rect_MouseLeftButtonDown");
         //   e.Handled = true;
        }

    }


  

}
    
    