﻿using RoomMaker.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RoomMaker.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private MainWindowModel Model { get; }
        private bool _canExecute { get; set; }


        public MainWindowViewModel()
        {
            Model = new MainWindowModel();
        }

        public bool IsControlOnCanvas
        {
            get { return Model.IsControlOnCanvas; }
            set
            {
                if (Model.IsControlOnCanvas == value) return;
                Model.IsControlOnCanvas = value;
                OnPropertyChanged("IsControlOnCanvas");

            }
        }

 

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

    }
}
