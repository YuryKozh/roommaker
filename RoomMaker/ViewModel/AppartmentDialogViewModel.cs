﻿using RoomMaker.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RoomMaker.ViewModel
{
    public class RelayCommand : ICommand
    {
        private Action action;
        private bool v;
        private Predicate<object> _canExecute;
        private Action<object> _execute;

        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            this._canExecute = canExecute;
            this._execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

      

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
    class AppartmentDialogViewModel:INotifyPropertyChanged
    {
        private ObservableCollection<NumberofRooms> items;

        public event PropertyChangedEventHandler PropertyChanged;
        private AppartmentDialogModel Model { get; }
        public ICommand AddLeft { get; protected set; }
        public ICommand RemoveLeft { get; protected set; }
        public ICommand AddUp { get; protected set; }
        public ICommand RemoveUp { get; protected set; }
        public ObservableCollection<StageAppartment> Tmp { get; set; }
        private int i ;
        public AppartmentDialogViewModel(ObservableCollection<NumberofRooms> items)
        {
            Model = new AppartmentDialogModel();
            this.items = items;
            StageBase = new ObservableCollection<StageAppartment>();
            i = items.Count;
            foreach(var x in items)
            {
                var temp = new ObservableCollection<ExtendedAppartmentEntity>();
                for (int j = 0; j < x.Number; j++)
                {
                    temp.Add(new ExtendedAppartmentEntity() {
                MarginDown=0,
                MarginUp=0,
                MarginRight=0,
                MarginLeft=0,
                Margin=new Thickness(0),
                AddLeft = new RelayCommand(TestTrue, AddMarginLeft),
                RemoveLeft = new RelayCommand(TestTrue, MinusMarginLeft),
                AddUp = new RelayCommand(TestTrue, AddMarginUp),
                RemoveUp = new RelayCommand(TestTrue, MinusMarginUp) });
                }

                StageBase.Add(new StageAppartment() { AppartmentBase = temp, StageNumber = i });
                i--;
            }
            //StageBase.Add(new StageAppartment()
            //{
            //    StageNumber = i,
            //    AppartmentBase = new ObservableCollection<ExtendedAppartmentEntity>() { new ExtendedAppartmentEntity()

            //    {
            //    MarginDown=0,
            //    MarginUp=0,
            //    MarginRight=0,
            //    MarginLeft=0,
            //    Margin=new Thickness(0),
            //    AddLeft = new RelayCommand(TestTrue, AddMarginLeft),
            //    RemoveLeft = new RelayCommand(TestTrue, MinusMarginLeft),
            //    AddUp = new RelayCommand(TestTrue, AddMarginUp),
            //    RemoveUp = new RelayCommand(TestTrue, MinusMarginUp)

            //} }
            //});
            //StageBase.Add(new StageAppartment()
            //{
            //    StageNumber = 2,
            //    AppartmentBase = new ObservableCollection<ExtendedAppartmentEntity>() { new ExtendedAppartmentEntity()
            //{
            //    MarginDown=0,
            //    MarginUp=0,
            //    MarginRight=0,
            //    MarginLeft=0,
            //    Margin=new Thickness(0),
            //    AddLeft = new RelayCommand(TestTrue, AddMarginLeft),
            //    RemoveLeft = new RelayCommand(TestTrue, MinusMarginLeft),
            //    AddUp = new RelayCommand(TestTrue, AddMarginUp),
            //    RemoveUp = new RelayCommand(TestTrue, MinusMarginUp),

            //},new ExtendedAppartmentEntity()
            //{
            //    MarginDown=0,
            //    MarginUp=0,
            //    MarginRight=0,
            //    MarginLeft=0,
            //    Margin=new Thickness(0),
            //    AddLeft = new RelayCommand(TestTrue, AddMarginLeft),
            //    RemoveLeft = new RelayCommand(TestTrue, MinusMarginLeft),
            //    AddUp = new RelayCommand(TestTrue, AddMarginUp),
            //    RemoveUp = new RelayCommand(TestTrue, MinusMarginUp) } }
            //});

          
        }

    

        static bool TestTrue(object x)
        {
            return true;
        }

        //public int MarginDown
   public ObservableCollection<StageAppartment> StageBase
        {
            get { return Model.StageBase; }
            set
            {
                if (Model.StageBase== value) return;
                Model.StageBase = value;
                OnPropertyChanged("StageBase");
            }
        }
        //public ObservableCollection<ExtendedAppartmentEntity> AppartmentBase
        //{
        //    get { return Model.Stage; }
        //    set
        //    {
        //        if (Model.AppartmentBase == value) return;
        //        Model.AppartmentBase = value;
        //        OnPropertyChanged("AppartmentBase");
        //    }
        //}

        private void AddMarginLeft(object sender)
        {
            var x = ((ExtendedAppartmentEntity)sender);
            x.MarginLeft++;
            x.Margin = new Thickness(x.MarginLeft, x.MarginUp, x.MarginRight, x.MarginDown);

            Tmp = StageBase;
            StageBase = new ObservableCollection<StageAppartment>();
            StageBase = Tmp;
        }
        private void MinusMarginLeft(object sender)
        {
            var x = ((ExtendedAppartmentEntity)sender);
            x.MarginLeft--;
             x.Margin = new Thickness(x.MarginLeft, x.MarginUp, x.MarginRight, x.MarginDown);
            Tmp = StageBase;
            StageBase = new ObservableCollection<StageAppartment>();
            StageBase = Tmp;
        }

        private void AddMarginUp(object sender)
        {
            var x = ((ExtendedAppartmentEntity)sender);
            x.MarginUp++;
            x.Margin = new Thickness(x.MarginLeft, x.MarginUp, x.MarginRight, x.MarginDown);
            Tmp = StageBase;
            StageBase = new ObservableCollection<StageAppartment>();
            StageBase = Tmp;
        }
        private void MinusMarginUp(object sender)
        {
            var x = ((ExtendedAppartmentEntity)sender);
            x.MarginUp--;
            x.Margin=new Thickness(x.MarginLeft, x.MarginUp,x.MarginRight,x.MarginDown);
            Tmp = StageBase;
            StageBase = new ObservableCollection<StageAppartment>();
            StageBase = Tmp;
        }



        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
