﻿using RoomMaker.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace RoomMaker.ViewModel
{
    class HouseDialogViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private HouseDialogModel Model { get; set; }
        private Regex Check =new Regex("(?:[0-9a-fA-F]{3}){1,2}$");

        public HouseDialogViewModel()
        {

            Model = new HouseDialogModel();

        }

        public string StageNumber
        {
            get { return Model.NumberOfStages; }
            set
            {
                if (Model.NumberOfStages == value) return;
                Model.NumberOfStages = value;
                IsFieldDigit=ValidateStageNumber();
                OnPropertyChanged("StageNumber");
            }
        }

        public bool IsFieldDigit
        {
            get { return Model.isFieldDigit; }
            set
            {
                if (Model.isFieldDigit == value) return;
                Model.isFieldDigit = value;
                OnPropertyChanged("IsFieldDigit");
                OnPropertyChanged("OkButton");
            }
        }

        public string StageColorString
        {
            get { return Model.StageColors; }
            set
            {
                if (Model.StageColors == value) return;
                Model.StageColors = value;
                OnPropertyChanged("StageColorString");
                if (StageColorString.Length == 6 && Check.IsMatch(StageColorString))
                {
                    OnPropertyChanged("StageColor");
                    IsCorrectBackground = true;

                }
                else
                {
                    IsCorrectBackground = false;
                }
            }
        }

        public string StageColor
        {
            get { return "#" + StageColorString; }

        }
        
        public bool IsCorrectBackground
        {
            get { return Model.IsCorrectBackground; }
            set
            {
                if (Model.IsCorrectBackground == value) return;
                Model.IsCorrectBackground = value;
                OnPropertyChanged("IsCorrectBackground");
                OnPropertyChanged("OkButton");
            }
        }

        public bool OkButton
        {
            get { return IsCorrectBackground & IsFieldDigit; }
        }


        public bool ValidateStageNumber()
        {
            if (String.IsNullOrEmpty(StageNumber))
                return false;
            foreach (var x in StageNumber)
            {
                if (!Char.IsDigit(x))
                    return false;
            }
            if (Convert.ToInt32(StageNumber) == 0)
                return false;
            return true;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
