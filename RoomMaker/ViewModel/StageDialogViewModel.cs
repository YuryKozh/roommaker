﻿using RoomMaker.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RoomMaker.ViewModel
{
    public class StageDialogViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private StageDialogModel Model { get; set; }

        public StageDialogViewModel()
        {
            Model = new StageDialogModel();
            Values = new ObservableCollection<NumberofRooms>();
          //  Values.CollectionChanged += OnCollectionChanged;
           
            
            }

        public ObservableCollection<NumberofRooms> Values
        {
            get { return Model.List; }
            set
            {
                if (Model.List == value) return;
                Model.List = value;
                OnPropertyChanged("Values");
            }
        }

        public bool Validation
        {
            get { return Model.ValidateFields; }
            set
            {
                if (Model.ValidateFields == value) return;
                Model.ValidateFields = value;
                OnPropertyChanged("Validation");
            }
        }



      public int Errors
        {
            get { return Model.Errors; }
            set {
                if (Model.Errors == value) return;
                Model.Errors = value;
                OnPropertyChanged("Errors");
            }
        }

      

        //void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.NewItems != null)
        //    {
        //        foreach (NumberofRooms newItem in e.NewItems)
        //        {
        //            //ModifiedItems.Add(newItem);

        //            //Add listener for each item on PropertyChanged event
        //            if (e.Action == NotifyCollectionChangedAction.Add)
        //                newItem.PropertyChanged += this.OnItemPropertyChanged;
        //            else if (e.Action == NotifyCollectionChangedAction.Remove)
        //                newItem.PropertyChanged -= this.OnItemPropertyChanged;
        //        }
        //    }
        //}

        //void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    var item = sender as NumberofRooms;
        //    if (item != null)
        //    {
        //        OnPropertyChanged("Values");
        //    }
        //}
        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }


    }

  
}
