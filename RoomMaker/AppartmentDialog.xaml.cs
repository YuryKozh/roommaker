﻿using RoomMaker.Model;
using RoomMaker.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RoomMaker
{
    /// <summary>
    /// Логика взаимодействия для AppartmentDialog.xaml
    /// </summary>
    public partial class AppartmentDialog : Window
    {
        private readonly AppartmentDialogViewModel _dialogviewmodel;
        public AppartmentDialog(ObservableCollection<NumberofRooms> Items)
        {
            InitializeComponent();
          //  _stageAppartmentList = new ObservableCollection<StageAppartment>();

            
            _dialogviewmodel = new AppartmentDialogViewModel(Items) {};
            DataContext = _dialogviewmodel;  
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

        }
        public ObservableCollection<StageAppartment> AppartmentLists
        {
            get { return _dialogviewmodel.StageBase; }
        }

    }
}
