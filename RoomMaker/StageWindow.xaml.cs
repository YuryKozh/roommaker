﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RoomMaker.Model;

namespace RoomMaker
{
    /// <summary>
    /// Логика взаимодействия для StageWindow.xaml
    /// </summary>
    public partial class StageWindow : Window
    {
        private List<ExtendedAppartmentEntity> appartmentList;

        public StageWindow(List<ExtendedAppartmentEntity> appartmentList)
        {
            this.appartmentList = appartmentList;
            InitializeComponent();
            foreach (var x in appartmentList)
            {
                var rectangle = new Rectangle() { Fill = Brushes.Coral, Width = 50, Height = 50, Margin = new Thickness(x.MarginLeft*10, x.MarginUp*10, x.MarginRight, x.MarginDown) };
                Grid.SetRow(rectangle, 0);
                Grid.SetColumn(rectangle, 0);
                StageGrid.Children.Add(rectangle);
            }

        }

    }
}
